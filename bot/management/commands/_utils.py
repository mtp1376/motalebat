from telegram import ReplyKeyboardRemove

from bot.management.commands._exceptions import BadMotalebeType
from bot.models import BotUser, Motalebe


def _get_or_create_user(user, chat):
    try:
        return BotUser.objects.get(telegram_id=user.id)
    except BotUser.DoesNotExist:
        return BotUser.objects.create(
            username=user.username,
            telegram_id=user.id,
            chat_id=chat.id,
            first_name=user.first_name,
            last_name=user.last_name,
        )


def _update_user_state(user, state):
    user.state = state
    user.save()


def _update_user_state_with_telegram_id(telegram_user_id, state):
    user = BotUser.objects.get(telegram_id=telegram_user_id)
    user.state = state
    user.save()


def _send_messsage(context, chat_id, text, reply_markup=None):
    context.bot.send_message(
        chat_id=chat_id,
        text=text,
        reply_markup=reply_markup or ReplyKeyboardRemove()
    )


def _get_motalebe_type(message):
    if message.text is not None:
        return Motalebe.Type.TEXT
    if message.voice is not None:
        return Motalebe.Type.VOICE
    raise BadMotalebeType()
