from telegram.ext import BaseFilter

from bot.models import BotUser


class MotalebeCommand(BaseFilter):
    def filter(self, update):
        try:
            return \
                BotUser.objects.get(
                    telegram_id=update.from_user.id
                ).state == BotUser.State.INITIAL.value
        except BotUser.DoesNotExist:
            return False


class MotalebeText(BaseFilter):
    def filter(self, update):
        try:
            return BotUser.objects.get(
                telegram_id=update.from_user.id
            ).state == BotUser.State.RECEIVE_MOTALEBE_MODE.value
        except BotUser.DoesNotExist:
            return False


class MotalebeContact(BaseFilter):
    def filter(self, update):
        try:
            return BotUser.objects.get(
                telegram_id=update.from_user.id
            ).state == BotUser.State.RECEIVE_MOTALEBE_CONTACT_MODE.value
        except BotUser.DoesNotExist:
            return False


motalebe_command_filter = MotalebeCommand()
motalebe_text_filter = MotalebeText()
motalebe_contact_filter = MotalebeContact()
