import telegram

from bot.management.commands._handlers import _show_commands
from bot.models import Motalebe, BotMessageTemplate


def minute_job(context: telegram.ext.CallbackContext):
    answers_to_send = Motalebe.objects.filter(should_send_answer=True).all()[:10]  # 10 messages per minute
    for this_answer in answers_to_send:
        context.bot.send_message(
            chat_id=this_answer.user.chat_id,
            text=BotMessageTemplate.objects.get(name='AnswerForMotalebeTemplate').content.format(
                answer=this_answer.answer,
            ),
            reply_to_message_id=this_answer.message_id,
        )
        this_answer.should_send_answer = False
        this_answer.save()
        _show_commands(context, this_answer.user.chat_id, this_answer.user.telegram_id)
