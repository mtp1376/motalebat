import logging
from os import getenv

from django.core.management.base import BaseCommand
from telegram.ext import CommandHandler
from telegram.ext import Updater, MessageHandler

from bot.management.commands._filters import motalebe_command_filter, \
    motalebe_text_filter, motalebe_contact_filter
from bot.management.commands._handlers import start, receive_motalebe, receive_motalebe_command, \
    receive_motalebe_contact
from bot.management.commands._jobs import minute_job


class Command(BaseCommand):
    help = 'Runs the bot!'

    def __init__(self):
        super().__init__()
        self.dispatcher = None

    @staticmethod
    def _set_logging():
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)

    def _register_handlers(self):
        start_handler = CommandHandler('start', start)
        self.dispatcher.add_handler(start_handler)

        receive_motalebe_command_handler = MessageHandler(motalebe_command_filter, receive_motalebe_command)
        self.dispatcher.add_handler(receive_motalebe_command_handler)

        receive_motalebe_handler = MessageHandler(motalebe_text_filter, receive_motalebe)
        self.dispatcher.add_handler(receive_motalebe_handler)

        receive_motalebe_contact_handler = MessageHandler(motalebe_contact_filter, receive_motalebe_contact)
        self.dispatcher.add_handler(receive_motalebe_contact_handler)

    def handle(self, *args, **options):
        updater = Updater(token=getenv('TELEGRAM_BOT_TOKEN'), use_context=True)
        job_queue = updater.job_queue

        self.dispatcher = updater.dispatcher
        self._set_logging()
        self._register_handlers()
        job_queue.run_repeating(minute_job, interval=60)

        try:
            updater.start_polling()
        except:
            updater.stop()
            raise
