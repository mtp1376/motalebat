from enum import Enum


class MotalebeCommands(Enum):
    SEND_MOTALEBE = 'ارسال مطالبه'
    MOTALEBE_FOLLOW_UP = 'پیگیری مطالبه'


DEFAULT_COMMANDS = [
    command.value for command in MotalebeCommands
]

ALL_COMMANDS = DEFAULT_COMMANDS
