from telegram import ReplyKeyboardMarkup

from bot.management.commands._exceptions import BadMotalebeType
from bot.management.commands._literals import MotalebeCommands, DEFAULT_COMMANDS
from bot.management.commands._utils import _get_or_create_user, _update_user_state_with_telegram_id, \
    _send_messsage, _get_motalebe_type
from bot.models import BotMessageTemplate, BotUser, Motalebe


def start(update, context):
    _send_messsage(
        context,
        update.effective_chat.id,
        BotMessageTemplate.objects.get(name='StartMessage').content
    )
    user = _get_or_create_user(update.effective_user, update.effective_chat)
    _show_commands(context, update.effective_chat.id, user.telegram_id)


def _show_commands(context, chat_id, user_telegram_id):
    commands = [DEFAULT_COMMANDS]
    _send_messsage(
        context,
        chat_id,
        BotMessageTemplate.objects.get(name='HowCanIHelp').content,
        reply_markup=ReplyKeyboardMarkup(
            keyboard=commands,
            one_time_keyboard=True
        )
    )
    _update_user_state_with_telegram_id(user_telegram_id, BotUser.State.INITIAL)


def _handle_user_command(update, context):
    if update.effective_message.text == MotalebeCommands.SEND_MOTALEBE.value:
        _send_messsage(
            context,
            update.effective_chat.id,
            BotMessageTemplate.objects.get(name='AskForMotalebe').content
        )
        _update_user_state_with_telegram_id(update.effective_user.id, BotUser.State.RECEIVE_MOTALEBE_MODE)
    else:
        _send_messsage(
            context,
            update.effective_chat.id,
            BotMessageTemplate.objects.get(name='UnimplementedCommand').content
        )
        _show_commands(context, update.effective_chat.id, update.effective_user.id)


def receive_motalebe_command(update, context):
    if update.effective_message.text in DEFAULT_COMMANDS:
        _handle_user_command(update, context)
    else:
        _send_messsage(
            context,
            update.effective_chat.id,
            BotMessageTemplate.objects.get(name='UnknownCommand').content
        )
        _show_commands(context, update.effective_chat.id, update.effective_user.id)


def receive_motalebe(update, context):
    motalebe = Motalebe.objects.create(
        user=BotUser.objects.get(telegram_id=update.effective_user.id),
        content=update.effective_message.text,
        message_id=update.effective_message.message_id,
    )
    try:
        motalebe.type = _get_motalebe_type(message=update.effective_message)
        motalebe.save()
    except BadMotalebeType:
        _send_messsage(
            context,
            update.effective_chat.id,
            BotMessageTemplate.objects.get(name='BadMotalebeType').content
        )
        return

    _update_user_state_with_telegram_id(
        update.effective_user.id, BotUser.State.RECEIVE_MOTALEBE_CONTACT_MODE
    )

    _send_messsage(
        context,
        update.effective_chat.id,
        BotMessageTemplate.objects.get(name='AskForMotalebeContact').content
    )


def receive_motalebe_contact(update, context):
    motalebe = Motalebe.objects.filter(
        user=BotUser.objects.get(telegram_id=update.effective_user.id),
    ).order_by('-created_at').first()
    motalebe.contact = update.effective_message.text
    motalebe.save()

    _send_messsage(
        context,
        update.effective_chat.id,
        BotMessageTemplate.objects.get(name='ThanksForMotalebe').content
    )

    _show_commands(context, update.effective_chat.id, update.effective_user.id)
