# Generated by Django 3.0.7 on 2020-06-28 15:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0007_auto_20200625_2029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='botuser',
            name='first_name',
            field=models.CharField(blank=True, max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='botuser',
            name='last_name',
            field=models.CharField(blank=True, max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='botuser',
            name='username',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
