from django.contrib import admin

# Register your models here.
from bot.models import BotMessageTemplate, BotUser, Motalebe
from django.utils.translation import ugettext_lazy as _


class BotMessageTemplateAdmin(admin.ModelAdmin):
    pass


class BotUserAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name', 'last_name', 'is_admin')


class MotalebeAdmin(admin.ModelAdmin):
    list_display = ('content', 'user', 'created_at_jalali', 'contact')

    def save_model(self, request, obj, form, change):
        obj.answered_by = request.user
        super().save_model(request, obj, form, change)


admin.site.site_header = _('Motalebot Management')
admin.site.site_title = _('Motalebot Management')
admin.site.index_title = _('Motalebot Management')

admin.site.register(BotMessageTemplate, BotMessageTemplateAdmin)
admin.site.register(BotUser, BotUserAdmin)
admin.site.register(Motalebe, MotalebeAdmin)
