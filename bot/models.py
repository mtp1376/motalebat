from datetime import datetime

import jdatetime
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class BotMessageTemplate(models.Model):
    class Meta:
        verbose_name = _('Bot Message Template')
        verbose_name_plural = _('Bot Message Template')

    name = models.CharField(max_length=30)
    content = models.TextField()

    def __str__(self):
        return self.name


class BotUser(models.Model):
    class Meta:
        verbose_name = _('Bot User')
        verbose_name_plural = _('Bot Users')

    class State(models.TextChoices):
        INITIAL = 'INITIAL', _('Initial Mode')
        RECEIVE_MOTALEBE_MODE = 'RECEIVE_MOTALEBE_MODE', _('Receive Motalebe Mode')
        RECEIVE_MOTALEBE_CONTACT_MODE = 'RECEIVE_MOTALEBE_CONTACT_MODE', _('Receive Motalebe Contact Mode')

    is_admin = models.BooleanField(default=False, verbose_name=_('Is Admin?'))
    username = models.CharField(max_length=30, null=True, blank=True, verbose_name=_('Username'))
    telegram_id = models.IntegerField(verbose_name=_('Telegram ID'))
    chat_id = models.IntegerField(verbose_name=_('Chat ID'))

    first_name = models.CharField(max_length=60, null=True, blank=True, verbose_name=_('First Name'))
    last_name = models.CharField(max_length=60, null=True, blank=True, verbose_name=_('Last Name'))

    state = models.CharField(max_length=30, choices=State.choices, default=State.INITIAL, verbose_name=_('State'))

    def __str__(self):
        return self.username or str(self.telegram_id)


class Motalebe(models.Model):
    class Meta:
        verbose_name = _('Motalebe')
        verbose_name_plural = _('Motalebes')

    class Type(models.TextChoices):
        VOICE = 'VOICE', _('Voice Message')
        TEXT = 'TEXT', _('Text Message')

    user = models.ForeignKey(BotUser, on_delete=models.CASCADE, verbose_name=_('Bot User'))
    created_at = models.DateTimeField(default=datetime.now, verbose_name=_('Created At'))
    content = models.TextField(null=True, verbose_name=_('Content'))
    contact = models.TextField(verbose_name=_('Contact'))
    type = models.CharField(max_length=10, choices=Type.choices, default=Type.TEXT, verbose_name=_('Message Type'))
    message_id = models.IntegerField(verbose_name=_('Message ID'))
    answered_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, verbose_name=_('Answered By'),
                                    blank=True)
    answer = models.TextField(null=True, verbose_name=_('Motalebe Answer'), blank=True)
    should_send_answer = models.BooleanField(default=False, verbose_name=_('Should Answer Be Sent?'))

    @property
    def created_at_jalali(self):
        if self.created_at:
            return jdatetime.date.fromgregorian(date=self.created_at).strftime('%Y/%m/%d')
        return '-'

    def __str__(self):
        return _('Motalebe from %(user)s in %(date)s') % {
            'user': str(self.user), 'date': self.created_at_jalali
        }

# TODO: LOG ALL THE MESSAGES SENT IN THE DATABASE
# class BotMessage(models.Model):
#     message = models.TextField()
#     user = models.ForeignKey(BotUser, on_delete=models.DO_NOTHING)
